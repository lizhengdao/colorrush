﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ExportImportPlayerPrefs : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Export(){
         File.Copy("/data/data/com.JUF.ColoRush/shared_prefs/com.JUF.ColoRush.v2.playerprefs.xml","/storage/emulated/0/Documents/ColoRushBackup.xml");
    }
    public void Import(){
         File.Copy("/storage/emulated/0/Documents/ColoRushBackup.xml","/data/data/com.JUF.ColoRush/shared_prefs/com.JUF.ColoRush.v2.playerprefs.xml");
    }
}
