﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class codeOn : MonoBehaviour {
public InputField Input;
public string zauberwort;
public GameObject Main;
public GameObject Code;
public GameObject wrong;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OkCheck(){
		string word = "";
		string many = "";
		try
		{
			word = Input.text.Substring(0,zauberwort.Length);
			Debug.Log(word);
			many = Input.text.Substring(zauberwort.Length,Input.text.Length-zauberwort.Length);
			Debug.Log(many);
		}
		catch (System.ArgumentOutOfRangeException)
		{
			wrong.SetActive(true);
		}
		int zahl = int.Parse(many);
		if(word == zauberwort){
			PlayerPrefs.SetInt("Gems", PlayerPrefs.GetInt("Gems")+zahl);
			Back();
		}
		else
		{
			wrong.SetActive(true);
		}
	}

	private void Back(){
		Main.SetActive(true);
		wrong.SetActive(false);
		Code.SetActive(false);
	}
}
