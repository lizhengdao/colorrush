﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateGemsBoss : MonoBehaviour {
public GameObject Gem;
private GameObject lol;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Generate(int Nr, int GemNr){
		Debug.Log("generate " + Nr);
		for(int i = 0; i < Nr;i++){
			float pos = Random.Range(-5F,5F);
			float pos2 = Random.Range(-5F,0.1F);
			float row;
			if(pos2 < -3.32)
				row = -5F;
			else if(pos2 < -1.66)
			{
				row = -2.5F;
			}
			else{
				row = 0;
			}

			lol = (GameObject)Instantiate (Gem, new Vector3(0,0,0), Quaternion.identity);
			lol.SetActive(true);
			lol.transform.position = new Vector3(row,1,10*(GemNr/Nr)+pos);
			lol.name = "gemT " + (GemNr+i);
		}
	}
}
