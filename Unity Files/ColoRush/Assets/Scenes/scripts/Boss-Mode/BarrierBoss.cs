﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierBoss : MonoBehaviour {
public GameObject sphere;
public GameObject GameController;
public bool Choosing;
public bool Daily;
public bool Multi;
// private GameObject Parent;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	void OnTriggerEnter(Collider other)
	{	
		if(Choosing && !Daily){
			if(other.gameObject.tag == "sphere"){
			if(this.name == sphere.GetComponent<SphereBoss>().color){
				Debug.Log("destroy... " + name);
				GameController.GetComponent<GameControllerChoose>().ScorePlus();
				// this.transform.parent.GetComponent<Under>().Call();
				// GameObject P = this.transform.parent.gameObject;
				// P.GetComponent<Under>().Call();
				// MyParentScript myScript = this.GetComponentInParent();
				// myScript.Call()

				Destroy(this);
			}
			else
			{
				Debug.Log("over " + name);
				GameController.GetComponent<GameControllerChoose>().gameOver();
			}
		}
		}
		else if(Daily){
			if(other.gameObject.tag == "sphere"){
			if(this.name == sphere.GetComponent<SphereBoss>().color){
				Debug.Log("destroy... " + name);
				GameController.GetComponent<GameControllerDy>().ScorePlus();

				Destroy(this);
			}
			else
			{
				Debug.Log("over " + name);
				GameController.GetComponent<GameControllerDy>().gameOver();
			}
		}
		}
		else if(Multi){
			if(other.gameObject.tag == "sphere"){
			if(this.name == sphere.GetComponent<SphereBoss>().color){
				Debug.Log("destroy... " + name);
				GameController.GetComponent<GameControllerMultiplayer>().ScorePlus();

				Destroy(this);
			}
			else
			{
				Debug.Log("over " + name);
				GameController.GetComponent<GameControllerMultiplayer>().gameOver();
				GameController.GetComponent<GameControllerMultiplayer>().Send("GameOver");
			}
		}
		}
		else{
		if(other.gameObject.tag == "sphere"){
			if(this.name == sphere.GetComponent<SphereBoss>().color){
				Debug.Log("destroy... " + name);
				GameController.GetComponent<GameControllerBoss>().ScorePlus();
				// this.transform.parent.GetComponent<Under>().Call();
				// GameObject P = this.transform.parent.gameObject;
				// P.GetComponent<Under>().Call();
				// MyParentScript myScript = this.GetComponentInParent();
				// myScript.Call()

				Destroy(this);
			}
			else
			{
				Debug.Log("over " + name);
				GameController.GetComponent<GameControllerBoss>().gameOver();
			}
		}
		}

	}
}
