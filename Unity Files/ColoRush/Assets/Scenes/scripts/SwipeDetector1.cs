﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDetector1 : MonoBehaviour
{
    private Vector2 fingerDown;
    private Vector2 fingerUp;
    public bool detectSwipeOnlyAfterRelease = false;

    private float SWIPE_THRESHOLD = 230f;
    public string Mode;

    // Update is called once per frame
    void Update()
    {

        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                fingerUp = touch.position;
                fingerDown = touch.position;
            }

            //Detects Swipe while finger is still moving
            if (touch.phase == TouchPhase.Moved)
            {
                if (!detectSwipeOnlyAfterRelease)
                {
                    fingerDown = touch.position;
                    checkSwipe();
                }
            }

            //Detects swipe after finger is released
            if (touch.phase == TouchPhase.Ended)
            {
                fingerDown = touch.position;
                checkSwipe();
            }
        }
         if(Input.anyKeyDown){
            if(Input.GetAxis("Horizontal") > 0){
                OnSwipeRight();
            }
            if(Input.GetAxis("Horizontal") < 0){
                OnSwipeLeft();
            }
            if(Input.GetAxis("yellow") != 0){
                colorAxis("yellow");
            }
            if(Input.GetAxis("red") != 0){
                colorAxis("red");
            }
            if(Input.GetAxis("blue") != 0){
                colorAxis("blue");
            }
        }


    }

    void checkSwipe()
    {
        if(PlayerPrefs.GetInt("SwipeOK") == 0){
            //Check if Vertical swipe
            if (verticalMove() > SWIPE_THRESHOLD && verticalMove() > horizontalValMove())
            {
                //Debug.Log("Vertical");
                if (fingerDown.y - fingerUp.y > 0)//up swipe
                {
                    OnSwipeUp();
                }
                else if (fingerDown.y - fingerUp.y < 0)//Down swipe
                {
                    OnSwipeDown();
                }
                fingerUp = fingerDown;
            }

            //Check if Horizontal swipe
            else if (horizontalValMove() > SWIPE_THRESHOLD && horizontalValMove() > verticalMove())
            {
                //Debug.Log("Horizontal");
                if (fingerDown.x - fingerUp.x > 0)//Right swipe
                {
                    OnSwipeRight();
                }
                else if (fingerDown.x - fingerUp.x < 0)//Left swipe
                {
                    OnSwipeLeft();
                }
                fingerUp = fingerDown;
            }

            //No Movement at-all
            else
            {
                //Debug.Log("No Swipe!");
            }
        }
    }

    float verticalMove()
    {
        return Mathf.Abs(fingerDown.y - fingerUp.y);
    }

    float horizontalValMove()
    {
        return Mathf.Abs(fingerDown.x - fingerUp.x);
    }

    //CALLBACK FUNCTIONS
	
	void OnSwipeUp()
    {
        Debug.Log("Swipe UP");
    }

    void OnSwipeDown()
    {
        Debug.Log("Swipe Down");
    }

    void OnSwipeLeft()
    {
        Debug.Log("Swipe Left");
        switch (Mode)
        {
            case "Running Mode":
                gameObject.GetComponent<GameControllerChoose>().swipe("Left");
                break;
            case "Boss Mode":
                gameObject.GetComponent<GameControllerBoss>().swipe("Left");
                break;
            case "Prof Mode":
                gameObject.GetComponent<GameControllerProf>().swipe("Left");
                break;
            case "Tutorial Mode":
                gameObject.GetComponent<TutorialGameController>().swipe("Left");
                break;
            case "Daily":
                gameObject.GetComponent<GameControllerDy>().swipe("Left");
                break;
            case "Multi":
                gameObject.GetComponent<GameControllerMultiplayer>().swipe("Left");
                break;
            default:
                break;
        }
    }

    void OnSwipeRight()
    {
        Debug.Log("Swipe Right");
		switch (Mode)
        {
            case "Running Mode":
                gameObject.GetComponent<GameControllerChoose>().swipe("Right");
                break;
            case "Boss Mode":
                gameObject.GetComponent<GameControllerBoss>().swipe("Right");
                break;
            case "Prof Mode":
                gameObject.GetComponent<GameControllerProf>().swipe("Right");
                break;
            case "Tutorial Mode":
                gameObject.GetComponent<TutorialGameController>().swipe("Right");
                break;
            case "Daily":
                gameObject.GetComponent<GameControllerDy>().swipe("Right");
                break;
            case "Multi":
                gameObject.GetComponent<GameControllerMultiplayer>().swipe("Right");
                break;
            default:
                break;
        }
    }

    void colorAxis(string color){
        Debug.Log("test");
        switch (Mode)
                    {
                        case "Running Mode":
                            gameObject.GetComponent<GameControllerChoose>().SphereColor(color);
                            break;
                        case "Boss Mode":
                            gameObject.GetComponent<GameControllerBoss>().SphereColor(color);
                            break;
                        case "Prof Mode":
                            gameObject.GetComponent<GameControllerProf>().SphereColor(color);
                            break;
                        case "Tutorial Mode":
                            gameObject.GetComponent<TutorialGameController>().SphereColor(color);
                            break;
                        case "Daily":
                            gameObject.GetComponent<GameControllerDy>().SphereColor(color);
                            break;
                        case "Endless":
                            gameObject.GetComponent<GameController>().SphereColor(color);
                            break;
                        case "Multi":
                            gameObject.GetComponent<GameControllerMultiplayer>().SphereColor(color);
                            break;
                        default:
                            break;
                    }
    }
}