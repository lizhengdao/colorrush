﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameControllerMultiplayer : MonoBehaviour {
private int cloneNr = 1;
private GameObject clone;
private GameObject clone2;
private GameObject clone3;
public float speedBefore;
private float PauseSpeed;
private float time;
private int GemNr = 0;
public int row;
private int line3Counter;
public string[] colorArray = new string[10000];
public int barrierNum = 0;
private float smoothFactor;

private bool side = true;
public bool smooth;
public float changeTime2;
private float time2;
private bool PauseOrGO = false;
private float seconds2 = 0;

public GameObject item;
public GameObject itemLeft;
public float speed = 1;
public GameObject sphere;
public GameObject spherePlayerTwo;

int rowPlayerTwo = 1;

public int score = 0;
public int HighScore = 0;
public Text scoreText; 
public Text HIText;
public Text GOText;
public Text PauseText;
public Text PauseGemText;
public GameObject GameOver;
public GameObject PauseButton;
public GameObject PauseObj;
public Text timer;
public int Gems;
public int GemScore = 0;
public Text GemText;
public string StandardColor;

public AudioSource[] Audios;
public int AudioFile;
public int SkinNum;

public GameObject[] grounds;
private int[] rows;
public GameObject Ziel;
public GameObject RunningObj;
public GameObject Buttons;
public GameObject[] ButtonsSide;
public GameObject ReachedZiel;
public Text RZScore;
public Text RZGems;
public Text RZAward;
public GameObject[] GemBonus;
public Text Title;
public Text pauseName;
public Text GOName;
private int Nr = 0;

// public bool level;
private bool reachedZielBool;

//PlayerPrefs
public bool ColorButtons;
public bool Running;
public int ManyRows;
public int length;
public int GemAward;
public GameObject levelObj;
public ServerClient SBL;
public GameObject ReadyQuestion;
public int SkinNumPlayerTwo = 0;


	// Use this for initialization
	void Start () {
		reachedZielBool = false;
		Debug.Log("s2 " + speed);

		// speed = speed + PlayerPrefs.GetFloat("SpeeDifference");
		// if(speed < 0 )
		// 	speed = 1;

		changeTime2 = PlayerPrefs.GetFloat("SideTime");
		smoothFactor = PlayerPrefs.GetFloat("Smooth");

		if(PlayerPrefs.GetInt("SmoothOn") == 1)
			smooth = true;
		else
			smooth = false;
		
		row = 1;
		SkinNum = PlayerPrefs.GetInt("SkinNum");
		AudioFile = PlayerPrefs.GetInt("AudioFile");
		HighScore = PlayerPrefs.GetInt("HighScoreMulti");

		if(PlayerPrefs.GetInt("LevelRun") == 1){
			Running = true;
			ColorButtons = false;
		}
		else if(PlayerPrefs.GetInt("LevelRun") == 2){
			Running = false;
			ColorButtons = true;
		}
		else{
			Running = false;
			ColorButtons = false;
		}

		ManyRows = PlayerPrefs.GetInt("LevelManyRows");
		if(ManyRows > 3)
			ManyRows = 3;
		GemAward = PlayerPrefs.GetInt("LevelGemAward");
		length = PlayerPrefs.GetInt("LevelLength");
		Debug.Log("Dyleng" + length);
		for(int i = 0; i < grounds.Length;i++){
    		grounds[i].SetActive(false);
		}

		grounds[0].SetActive(true);
		grounds[1].SetActive(true);

		if(ManyRows > 1){
			grounds[2].SetActive(true);
			grounds[3].SetActive(true);
		}
		
		if(ManyRows >= 3){
			grounds[4].SetActive(true);
			grounds[5].SetActive(true);
		}

		if(length % 2 != 0)
			length++;

		if(ColorButtons && Running)
			Debug.Log("Hier läuft was schieft, ColorButtons und Running sind true");

		if(Running && !ColorButtons){
			Buttons.SetActive(false);
			RunningObj.SetActive(true);
			colorArray = generateLevel.Changing(length, PlayerPrefs.GetString("LevelHash"));
			rows = generateLevel.ChangingRow(length, PlayerPrefs.GetString("LevelHash"), ManyRows);
			for (int i = 0; i < rows.Length; i++)
			{
				Debug.Log("row " + rows[i]);
			}
			StandardColor = colorArray[0];
		}
		else if(ColorButtons){
			colorArray = generateLevel.ColorRows(3, length, PlayerPrefs.GetString("LevelHash"));
			Buttons.SetActive(true);
			RunningObj.SetActive(false);
			if(ManyRows == 1){
				ButtonsSide[0].SetActive(false);
				ButtonsSide[1].SetActive(false);
			}
			else{
				ButtonsSide[0].SetActive(true);
				ButtonsSide[1].SetActive(true);
			}
		}
		else if(!Running && !ColorButtons){
			colorArray = generateLevel.ColorRows(3, length, PlayerPrefs.GetString("LevelHash"));
			StandardColor = colorArray[0];
			spherePlayerTwo.SetActive(false);
			ColorButtons = true;

			if(PlayerPrefs.GetInt("LevelRun")==3){
				Buttons.SetActive(false);
				RunningObj.SetActive(true);
			}
			else{
				PlayerPrefs.SetInt("SwipeOK",1);
				Buttons.SetActive(true);
				RunningObj.SetActive(false);
				ButtonsSide[0].SetActive(false);
				ButtonsSide[1].SetActive(false);
				changeTime2 = float.MaxValue;
				ButtonSide();
			}
		}

		Intialize(length);
		Debug.Log(length*5);

		if(ManyRows == 1)
			Ziel.transform.position = new Vector3(1.5f-5f,Ziel.transform.position.y,(length*5)-13f);
		else if(ManyRows == 2)
			Ziel.transform.position = new Vector3(0.1f-5f,Ziel.transform.position.y,(length*5)-13f);
		else
			Ziel.transform.position = new Vector3(-1f-5f,Ziel.transform.position.y,(length*5)-13f);
		Audios[AudioFile].GetComponent<AudioSource>().Play(0);
		Audio();
		speedBefore = speed;
		Debug.Log("speed " + speed);

		SphereColor(StandardColor);


		// pauseName.text = "Level " + PlayerPrefs.GetInt("LevelNum");
		// GOName.text = "Level " + PlayerPrefs.GetInt("LevelNum");
		
	}
	
	// Update is called once per frame
	void Update () {
		Audio();
        // checkSpherePlayerTwo();
		if(Running)
			SphereColor(StandardColor);

		time += Time.deltaTime;
		PauseText.text = "" + score;
		PauseGemText.text = "" + GemScore;
		sphere.transform.position += new Vector3(0,0,Time.deltaTime * speed);
		spherePlayerTwo.transform.localPosition += new Vector3(0,0,Time.deltaTime * speed);

		time2 += Time.deltaTime;
		seconds2 = time2 % 60;

		//PauseOrGo einbauen,
		//Swipe bool einrichten

		if(!PauseOrGO){
			timer.text = "" + seconds2;
			if(seconds2 <= changeTime2)
				side = false;
			else{
				side = true;}
		}

		if (score > HighScore)
		{
			HighScore =  score;
		}
		
		scoreText.text = score + " ";
		HIText.text = " HI " + HighScore;

	    PlayerPrefs.SetInt("HighScoreMulti", HighScore);
		
	}

	void OnApplicationPause(bool pauseStatus){
		if(pauseStatus && !reachedZielBool){
			PauseObj.SetActive(true);
			PauseButton.SetActive(false);
			Pause();
		}
	}

	
    void SendStatus(){
		if(PlayerPrefs.GetInt("LevelRun")==3){
			;
		}
		else{
			string color = sphere.GetComponent<SphereBoss>().color;
			string status = "";
			if(color.Length > 0){
				status = "C" +  color[0] + " ";
				Debug.Log("send " + status);
				SBL.send(status);
			}
		}
    }

	public void speedBeforeNull(){
		speedBefore = 0;
	}
	public void SendStart(){
		SBL.send("S");
		FirstStart();
	}

	public void Send(string status){
		SBL.send(status); //GoOn, GameOver (G), Pause (P), End, NewGame
	}

	void SendStatus(char direction){
		if(PlayerPrefs.GetInt("LevelRun")==4){
			;
		}
		else{
			string color = sphere.GetComponent<SphereBoss>().color;
			string status = "";
			if(color.Length > 0){
				status = "C" +  color[0] + direction;
				Debug.Log("send " + status);
				SBL.send(status);}
		}
	}
    void checkSpherePlayerTwo(string color, string direction){
		Debug.Log(color);
		Debug.Log(direction);
		if(PlayerPrefs.GetInt("LevelRun") == 3){
			SphereColorManual(color);
		}
		else if(PlayerPrefs.GetInt("LevelRun") == 4){
			side = true;
			if(direction != " ")
				swipe(direction);
		}
		else{
			SphereColorPlayerTwo(color);
			if(direction != " ")
				swipePlayerTwo(direction);
		}
        
    }


    public void InputStringBluetooth(string content){
        string color = "";
        string direction = "";

        //getting color of sphere two
        if(content[0] == 'C'){
            switch (content[1])
            {
                case 'r':
                    color = "red";
                    break;
                case 'y':
                    color = "yellow";
                    break;
                case 'b':
                    color = "blue";
                    break;
                case 'p':
                    color = "purple";
                    break;
                case 'g':
                    color = "green";
                    break;
                case 'o':
                    color = "orange";
                    break;
                default:
                    break;
            }

            //getting row
            switch (content[2])
            {
                case 'l':
                    direction = "Left";
                    break;
                case 'r':
                    direction = "Right";
                    break;
                default:
					direction = " ";
                    break;
            }
			checkSpherePlayerTwo(color, direction);
        }
        else if(content == "GameOver"){
			GameOver.SetActive(true);
            gameOver();

        }
        else if(content == "Pause"){
			PauseObj.SetActive(true);
            Pause();
        }
		else if(content == "S"){
			FirstStart();
		}
		else if(content == "GoOn"){
			PauseObj.SetActive(false);
			GoOn();
		}
		else if(content == "End"){
			spherePlayerTwo.SetActive(false);
		}
		else if(content == "NewGame"){
			Reset();
		}
		Debug.Log(direction);

    }

	void FirstStart(){
		ReadyQuestion.SetActive(false);
		SendStatus();
		GoOn();
		speedBefore = speed;
	}

	public void swipePlayerTwo(string direction){
			if(smooth){
				switch (direction)
				{
					case "Left":
					if(rowPlayerTwo == 1 && rowPlayerTwo < ManyRows){
						StartCoroutine (smooth_moveLeftTwo ("Left", smoothFactor, -2.6F));
						rowPlayerTwo = 2;
					}
					else if(rowPlayerTwo == 2 && rowPlayerTwo < ManyRows){
						StartCoroutine (smooth_moveLeftTwo ("Left", smoothFactor, -5.1F));
						rowPlayerTwo = 3;
					}
						break;
					case "Right":
						if(rowPlayerTwo == 3){
						StartCoroutine (smooth_moveLeftTwo ("Right", smoothFactor, -2.6F));
						rowPlayerTwo = 2;
						}
						else if(rowPlayerTwo == 2){
							StartCoroutine (smooth_moveLeftTwo ("Right", smoothFactor, 0));
							rowPlayerTwo = 1;
						}
						break;
					default:
						break;
				}
			}
			else{
				Debug.Log(rowPlayerTwo);
				Debug.Log(direction);
				switch (direction)
				{
					case "Left":
					if(rowPlayerTwo == 1 && rowPlayerTwo < ManyRows){
						spherePlayerTwo.transform.localPosition = new Vector3(-2.6f, spherePlayerTwo.transform.localPosition.y, spherePlayerTwo.transform.localPosition.z);
						rowPlayerTwo = 2;
					}
					else if(rowPlayerTwo == 2 && rowPlayerTwo < ManyRows){
						spherePlayerTwo.transform.localPosition = new Vector3(-5.1f, spherePlayerTwo.transform.localPosition.y, spherePlayerTwo.transform.localPosition.z);
						rowPlayerTwo = 3;
					}
						break;
					case "Right":
						if(rowPlayerTwo == 3){
                            spherePlayerTwo.transform.localPosition = new Vector3(-2.6f, spherePlayerTwo.transform.localPosition.y, spherePlayerTwo.transform.localPosition.z);
                            rowPlayerTwo = 2;
						}
						else if(rowPlayerTwo == 2){
							spherePlayerTwo.transform.localPosition = new Vector3(0, spherePlayerTwo.transform.localPosition.y, spherePlayerTwo.transform.localPosition.z);
							rowPlayerTwo = 1;
						}
						break;
					default:
						break;
				}
		}
	}

	 IEnumerator smooth_moveLeftTwo(string direct,float speed, float normal){
		 Vector3 direction;
         float startime = Time.time;
         Vector3 start_pos = spherePlayerTwo.transform.localPosition; //Starting position.
         Vector3 end_pos; //Ending position.
		if(direct  == "Left"){
			direction  = Vector3.left;
			end_pos = spherePlayerTwo.transform.localPosition + direction;
			while (start_pos != end_pos && ((Time.time - startime)*speed) < 1f && spherePlayerTwo.transform.localPosition.x > normal) { 
				float move = Mathf.Lerp (0,1, (Time.time - startime)*speed);
	
				spherePlayerTwo.transform.localPosition += direction*move;
	
				yield return null;
			}
		 }
		else{
			direction = Vector3.right;
			end_pos = spherePlayerTwo.transform.localPosition + direction;
			while (start_pos != end_pos && ((Time.time - startime)*speed) < 1f && spherePlayerTwo.transform.localPosition.x < normal) { 
				float move = Mathf.Lerp (0,1, (Time.time - startime)*speed);
	
				spherePlayerTwo.transform.localPosition += direction*move;
	
				yield return null;
			}
		}
		 spherePlayerTwo.transform.localPosition = new Vector3(normal, spherePlayerTwo.transform.localPosition.y, spherePlayerTwo.transform.localPosition.z);
     }

    public void generate (string color1, string color2, string color1l, string color2l, string color1ll, string color2ll, int row1, int row2, string standColor, string standColor2){
		clone = (GameObject)Instantiate (item, new Vector3(0,0,0), Quaternion.identity);
		clone.SetActive(true);
		if(row1 == 0){
			if(row2 == 0)
				clone.GetComponent<ItemBoss>().colorChange(standColor, standColor2, cloneNr, cloneNr+1);
			else
				clone.GetComponent<ItemBoss>().colorChange(standColor, color2, cloneNr, cloneNr+1);
			}
		else if(row2 == 0){
			clone.GetComponent<ItemBoss>().colorChange(color1, standColor2, cloneNr, cloneNr+1);
		}
		else
			clone.GetComponent<ItemBoss>().colorChange(color1, color2, cloneNr, cloneNr+1);
		clone.transform.position = new Vector3(0,0,cloneNr*5);
		clone.name = "clone" + " " + cloneNr;
		// clone.GetComponent<ItemBoss>().generateGems(Gems, GemNr);

		if(ManyRows > 1){
			clone2 = (GameObject)Instantiate (itemLeft, new Vector3(0,0,0), Quaternion.identity);
			clone2.SetActive(true);
			if(row1 == 1){
				if(row2 == 1)
					clone2.GetComponent<ItemBoss>().colorChange(standColor, standColor2, 0, 0);
				else
					clone2.GetComponent<ItemBoss>().colorChange(standColor, color2l, 0, 0);
				}
			else if(row2 == 1){
				clone2.GetComponent<ItemBoss>().colorChange(color1l, standColor2, 0, 0);
			}
			else
				clone2.GetComponent<ItemBoss>().colorChange(color1l, color2l, 0, 0);
			clone2.transform.position = new Vector3(-2.6F,0,cloneNr*5);
			clone2.name = "clone" + " " + (cloneNr-1);
			
		}

		if (ManyRows > 2){

			clone3 = (GameObject)Instantiate (itemLeft, new Vector3(0,0,0), Quaternion.identity);
			clone3.SetActive(true);
			if(row1 == 2){
				if(row2 == 2)
					clone3.GetComponent<ItemBoss>().colorChange(standColor, standColor2, 0, 0);
				else
					clone3.GetComponent<ItemBoss>().colorChange(standColor, color2ll, 0, 0);
				}
			else if(row2 == 2){
				clone3.GetComponent<ItemBoss>().colorChange(color1ll, standColor2, 0, 0);
			}
			else
				clone3.GetComponent<ItemBoss>().colorChange(color1ll, color2ll, 0, 0);
			clone3.transform.position = new Vector3(-5.1F,0,cloneNr*5);
			clone3.name = "cloneT " + line3Counter;
			line3Counter++;
			clone3.GetComponent<ItemBoss>().generateGems(Gems, GemNr);
			
			GemNr+= Gems;

		}

		cloneNr += 2;
	}

	public void SphereColor(string color){
		StandardColor = color;
		if(Running){
			var seconds = time % 60;
			timer.text = "" + seconds;
			switch (color)
			{
				case "yellow":
					sphere.GetComponent<SphereBoss>().color = "yellow";
					break;
				case "red":
					sphere.GetComponent<SphereBoss>().color = "red";
					break;
				case "blue":
					sphere.GetComponent<SphereBoss>().color = "blue";
					break;
				case "orange":
					sphere.GetComponent<SphereBoss>().color = "orange";
					break;
				case "purple":
					sphere.GetComponent<SphereBoss>().color = "purple";
					break;
				case "green":
					sphere.GetComponent<SphereBoss>().color = "green";
					break;
				default:
					sphere.GetComponent<SphereBoss>().color = "white";
					break;
			}
			time = 0;
		}
		else{
			time2 = 0;
			var seconds = time % 60;
			float changeTime = 0.5F;
			switch (color)
			{
				case "yellow":
					if(seconds <= changeTime){
						Debug.Log(seconds);
						sphere.GetComponent<SphereBoss>().doubleColor("yellow");
						time = 0;
					}
					else{
						sphere.GetComponent<SphereBoss>().color = "yellow";
					}
					break;
				case "blue":
					if(seconds <= changeTime){
						sphere.GetComponent<SphereBoss>().doubleColor("blue");
						time = 0;
					}
					else{
						sphere.GetComponent<SphereBoss>().color = "blue";
					}
					break;
				case "red":
					if(seconds <= changeTime){
						sphere.GetComponent<SphereBoss>().doubleColor("red");
						time = 0;
					}
					else{
						sphere.GetComponent<SphereBoss>().color = "red";
					}
					break;
				default:
				sphere.GetComponent<SphereBoss>().color = "white";
					break;
			}
			time = 0;
			SendStatus();
		}
	}

    public void SphereColorPlayerTwo(string color){
			switch (color)
			{
				case "yellow":
					spherePlayerTwo.GetComponent<SphereBoss>().color = "yellow";
					break;
				case "red":
					spherePlayerTwo.GetComponent<SphereBoss>().color = "red";
					break;
				case "blue":
					spherePlayerTwo.GetComponent<SphereBoss>().color = "blue";
					break;
				case "orange":
					spherePlayerTwo.GetComponent<SphereBoss>().color = "orange";
					break;
				case "purple":
					spherePlayerTwo.GetComponent<SphereBoss>().color = "purple";
					break;
				case "green":
					spherePlayerTwo.GetComponent<SphereBoss>().color = "green";
					break;
				default:
					spherePlayerTwo.GetComponent<SphereBoss>().color = "white";
					break;
			}
	}

	public void SphereColorManual(string color){
			switch (color)
			{
				case "yellow":
					sphere.GetComponent<SphereBoss>().color = "yellow";
					break;
				case "red":
					sphere.GetComponent<SphereBoss>().color = "red";
					break;
				case "blue":
					sphere.GetComponent<SphereBoss>().color = "blue";
					break;
				case "orange":
					sphere.GetComponent<SphereBoss>().color = "orange";
					break;
				case "purple":
					sphere.GetComponent<SphereBoss>().color = "purple";
					break;
				case "green":
					sphere.GetComponent<SphereBoss>().color = "green";
					break;
				default:
					sphere.GetComponent<SphereBoss>().color = "white";
					break;
			}
	}

	public void gameOver(){
		PauseOrGO = true;
		side = false;
		if(score > HighScore)
			HighScore = score;
		speed = 0;
		GameOver.SetActive(true);
		PauseButton.SetActive(false);
		GOText.text = "" + score;
		GemText.text = "" + GemScore;
		int gScore = PlayerPrefs.GetInt("Gems")+GemScore;
		PlayerPrefs.SetInt("Gems",gScore);
	}

	public void Reset(){
		Nr = 0;
		barrierNum = 0;
		ReachedZiel.SetActive(false);
		GameOver.SetActive(false);
		PauseButton.SetActive(true);
		side = true;
		PauseOrGO = false;
		score = 0;
		sphere.transform.position = new Vector3(0.02F, 0.5F, -26F);
        spherePlayerTwo.transform.localPosition = new Vector3(0.02F, 0.5F, -26F);
		row = 1;
        rowPlayerTwo = 1;
		DeleteBarriers();
		speed = speedBefore;
		// speed = speed - PlayerPrefs.GetFloat("SpeeDifference");
		cloneNr = 1;
		line3Counter = 0;
		GemScore = 0;
		// StandardColor = getRandomColor();
		// colorArray = new string[10000];
		Start();
		Audios[AudioFile].GetComponent<AudioSource>().Play(0);
		}

	public void ScorePlus(){

		barrierNum++;
		if(Running)
			StandardColor = colorArray[barrierNum];
		score++;
		if(score < 10){
			speed += PlayerPrefs.GetFloat("MultiSpeedAddition");
			smoothFactor += PlayerPrefs.GetFloat("MultiSpeedAddition");
		}
		else if(score < 100){
			speed += PlayerPrefs.GetFloat("MultiSpeedAddition") * 0.1f;
			smoothFactor += PlayerPrefs.GetFloat("MultiSpeedAddition") * 0.1f;
		}
		else if(score > 100){
			speed += PlayerPrefs.GetFloat("MultiSpeedAddition") * 0.01f;
			smoothFactor += PlayerPrefs.GetFloat("MultiSpeedAddition") * 0.01f;
		}
	}

	public void DeleteBarriers(){
		for(int i = 0; i <= cloneNr; i++){
			GameObject gb = GameObject.Find("clone " + i);
			Destroy(gb);
		}
		for (int i = 0; i <= line3Counter ; i++)
		{
			GameObject gb = GameObject.Find("cloneT " + i);
			Destroy(gb);
		}
		cloneNr = 1;
		for(int y = 0; y <= GemNr; y++){
			GameObject gb = GameObject.Find("gemT " + y);
			Destroy(gb);
		}
		GemNr = 0;
	}

	public void Pause(){
		PauseOrGO = true;
		side = false;
		PauseSpeed = speed;
		speed = 0;
	}

	public void GoOn(){
		PauseOrGO = false;
		side  = true;
		speed = PauseSpeed;
	}

	public void Intialize(int Number){
		for(int i = 0; i < Number/2;i++){
		generateRandColor();
		}
	}

	public void swipe(string direction){
		if(side || PlayerPrefs.GetInt("LevelRun")==4){
			Debug.Log("send status");
			if(PlayerPrefs.GetInt("LevelRun")!=4)
				SendStatus(char.ToLower(direction[0]));
			if(smooth){
				switch (direction)
				{
					case "Left":
					if(row == 1 && row < ManyRows){
						StartCoroutine (smooth_moveLeft ("Left", smoothFactor, -2.6F));
						row = 2;
					}
					else if(row == 2 && row < ManyRows){
						StartCoroutine (smooth_moveLeft ("Left", smoothFactor, -5.1F));
						row = 3;
					}
						break;
					case "Right":
						if(row == 3){
						StartCoroutine (smooth_moveLeft ("Right", smoothFactor, -2.6F));
						row = 2;
						}
						else if(row == 2){
							StartCoroutine (smooth_moveLeft ("Right", smoothFactor, 0));
							row = 1;
						}
						break;
					default:
						break;
				}
			}
			else{
				switch (direction)
				{
					case "Left":
					if(row == 1 && row < ManyRows){
						sphere.transform.position = new Vector3(-2.6f, sphere.transform.position.y, sphere.transform.position.z);
						row = 2;
					}
					else if(row == 2 && row < ManyRows){
						sphere.transform.position = new Vector3(-5.1f, sphere.transform.position.y, sphere.transform.position.z);
						row = 3;
					}
						break;
					case "Right":
						if(row == 3){
						sphere.transform.position = new Vector3(-2.6f, sphere.transform.position.y, sphere.transform.position.z);
						row = 2;
						}
						else if(row == 2){
							sphere.transform.position = new Vector3(0, sphere.transform.position.y, sphere.transform.position.z);
							row = 1;
						}
						break;
					default:
						break;
				}
		}
		}
	}

	 IEnumerator smooth_moveLeft(string direct,float speed, float normal){
		 Vector3 direction;
         float startime = Time.time;
         Vector3 start_pos = sphere.transform.position; //Starting position.
         Vector3 end_pos; //Ending position.
		if(direct  == "Left"){
			direction  = Vector3.left;
			end_pos = sphere.transform.position + direction;
			while (start_pos != end_pos && ((Time.time - startime)*speed) < 1f && sphere.transform.position.x > normal) { 
				float move = Mathf.Lerp (0,1, (Time.time - startime)*speed);
	
				sphere.transform.position += direction*move;
	
				yield return null;
			}
		 }
		else{
			direction = Vector3.right;
			end_pos = sphere.transform.position + direction;
			while (start_pos != end_pos && ((Time.time - startime)*speed) < 1f && sphere.transform.position.x < normal) { 
				float move = Mathf.Lerp (0,1, (Time.time - startime)*speed);
	
				sphere.transform.position += direction*move;
	
				yield return null;
			}
		}
		 sphere.transform.position = new Vector3(normal, sphere.transform.position.y, sphere.transform.position.z);
     }
	public string getRandomColor(){
		int col1 = Random.Range(1,7);
		string c1 = "";
		switch (col1)
		{
			case 1:
				c1 = "yellow";
				break;
			case 2:
				c1 = "red";
				break;
			case 3:
				c1 = "blue";
				break;
			case 4:
				c1 = "purple";
				break;
			case 5:
				c1 = "orange";
				break;
			case 6:
				c1 = "green";
				break;
			default:
				break;
		}
		return c1;
	}

	public void Mute(){
		int vol = PlayerPrefs.GetInt("Volume");
		if(vol - 1 == 0)
			vol = 0;
		else
			vol = 1;
		PlayerPrefs.SetInt("Volume", vol);
	}

	public void Audio(){
		int vol = PlayerPrefs.GetInt("Volume");
		if(vol == 0)
			Audios[AudioFile].GetComponent<AudioSource>().mute = true;
		else
			Audios[AudioFile].GetComponent<AudioSource>().mute = false;
	}

	public void generateRandColor(){
		int rand1;
		int rand2;
		string coloR1;
		string coloR2;
		string coloR1l;
		string coloR2l;
		string coloR1ll;
		string coloR2ll;

		if(Running && !ColorButtons){
			rand1 = rows[Nr];
			rand2 = rows[Nr+1];
			Nr += 2;
			coloR1 = getRandomColor();
			while(coloR1 == colorArray[cloneNr-1])
				coloR1 = getRandomColor();
			coloR2 = getRandomColor();
			while(coloR2 == colorArray[cloneNr])
				coloR2 = getRandomColor();
			coloR1l = getRandomColor();
			while(coloR1l == colorArray[cloneNr-1])
				coloR1l = getRandomColor();
			coloR2l = getRandomColor();
			while(coloR2l == colorArray[cloneNr])
					coloR2l = getRandomColor();
			coloR1ll = getRandomColor();
			while(coloR1ll == colorArray[cloneNr-1])
				coloR1ll = getRandomColor();
			coloR2ll = getRandomColor();
			while(coloR2ll == colorArray[cloneNr])
				coloR2ll = getRandomColor();
		}
		else{
			rand1 = 4;
			rand2 = 4;
			coloR1 = colorArray[cloneNr-1];
			coloR2 = colorArray[cloneNr];
			coloR1l = colorArray[cloneNr+1];
			coloR2l = colorArray[cloneNr+2];
			coloR1ll = colorArray[cloneNr+3];
			coloR2ll = colorArray[cloneNr+4];

		}

		Debug.Log("1 "+ rand1);
		Debug.Log("2 " +rand2);

			generate(coloR1, coloR2, coloR1l, coloR2l, coloR1ll, coloR2ll, rand1, rand2, colorArray[cloneNr-1], colorArray[cloneNr]);
		}	

	public int returnScore(){
		return score;
	}

	public void ZielErreicht(){
		reachedZielBool = true;
		Debug.Log("Toooolllll ... Ziel erreicht!");
		PauseOrGO = true;
		PauseButton.SetActive(false);
		side = false;
		if(score > HighScore)
			HighScore = score;
		speed = 0;
		ReachedZiel.SetActive(true);
		if(GemAward == 0){
			for (int i = 0; i < GemBonus.Length; i++)
			{
				GemBonus[i].SetActive(false);
			}
		}
		RZScore.text = "" + score;
		RZGems.text = "" + GemScore;
		RZAward.text = "" + GemAward;
		Title.text = "Ihr habt es gemeinsam geschafft!!!";
		GemScore += GemAward;
		int gScore = PlayerPrefs.GetInt("Gems")+GemScore;
		PlayerPrefs.SetInt("Gems",gScore);
	}

	public void nextLevel(){
		if(PlayerPrefs.GetInt("LevelNum") >= 21)
			;
		else
			levelObj.GetComponent<Level>().SelectLevel(PlayerPrefs.GetInt("LevelNum")+1);
	}

	public void ButtonSide(){
		side = false;
	}
}
