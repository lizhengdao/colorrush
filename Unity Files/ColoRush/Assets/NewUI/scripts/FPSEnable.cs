﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSEnable : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerPrefs.GetInt("FPS") == 1){
            // active.SetActive(true);
            // deactive.SetActive(false);
            Button b = gameObject.GetComponent(typeof(Button)) as Button;
            ColorBlock colors = b.colors;
            colors.normalColor = Color.green;
            colors.highlightedColor = Color.green;
            b.colors = colors;
            }
        else{
            // active.SetActive(false);
            // deactive.SetActive(true);
            Button b = gameObject.GetComponent(typeof(Button)) as Button;
            ColorBlock colors = b.colors;
            colors.normalColor = Color.red;
            colors.highlightedColor = Color.red;
            b.colors = colors;
            }
    }
}
