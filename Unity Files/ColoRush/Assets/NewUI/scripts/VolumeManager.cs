﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeManager : MonoBehaviour
{
   public AudioSource TitleMusic;
   public GameObject[] VolumePictures;
	// Use this for initialization
	void Start () {
		TitleMusic.Play(0);
	}
	
	// Update is called once per frame
	void Update () {
		
		Audio();
		RevertChoose();
	}
	public void Mute(){
		int vol = PlayerPrefs.GetInt("Volume");
		if(vol - 1 == 0)
			vol = 0;
		else
			vol = 1;
		PlayerPrefs.SetInt("Volume", vol);
	}

	private void Audio(){
		int vol = PlayerPrefs.GetInt("Volume");
		if(vol == 0){
			VolumePictures[0].SetActive(false);
            VolumePictures[1].SetActive(true);
			TitleMusic.GetComponent<AudioSource>().mute = true;
            }
		else{
			VolumePictures[1].SetActive(false);
            VolumePictures[0].SetActive(true);
			TitleMusic.GetComponent<AudioSource>().mute = false;
            }
	}

	private void RevertChoose(){
		PlayerPrefs.SetInt("chosen", 0);
	}
}
