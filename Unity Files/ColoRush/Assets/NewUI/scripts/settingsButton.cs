﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class settingsButton : MonoBehaviour
{
    public GameObject[] Settings;
    bool showComplete;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ButtonClick(){
        showComplete = !showComplete;
        if(showComplete){
            for (int i = 0; i < Settings.Length; i++)
            {
                Settings[i].SetActive(true);
            }
        }
        else{
            for (int i = 0; i < Settings.Length; i++)
            {
                Settings[i].SetActive(false);
            }
        }
        // this.transform.rotation
    }
}
