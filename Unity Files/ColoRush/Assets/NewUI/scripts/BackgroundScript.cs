﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScript : MonoBehaviour
{
public int SkinNr;
public GameObject[] disable;
public GameObject Main;
public GameObject leftDown;
public GameObject rightDown;
public GameObject leftUp;
public GameObject rightUp;
public bool Onleft;
public bool down;
    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.GetInt("Tutorial") == 0)
            PlayerPrefs.SetInt("Backgrounds",4);
    }

    // Update is called once per frame
    void Update()
    {
        int skin = PlayerPrefs.GetInt("Backgrounds");;
		if(skin == SkinNr){
			if(Onleft && down){
				leftDown.SetActive(true);
				}
			else if(down){
				rightDown.SetActive(true);
			}
			else if(Onleft){
				leftUp.SetActive(true);
			}
			else{
				rightUp.SetActive(true);
				}
		}
		else{
			if(Onleft && down){
				leftDown.SetActive(false);
				}
			else if(down){
				rightDown.SetActive(false);
			}
			else if(Onleft){
				leftUp.SetActive(false);
			}
			else{
				rightUp.SetActive(false);
				}
		}
		
    }

    public void Select(){
        PlayerPrefs.SetInt("Backgrounds", SkinNr);
        Close();
    }

    private void Close(){
		for(int i = 0; i < disable.Length;i++){
			disable[i].SetActive(false);
		}
		Main.SetActive(true);

	}
}
