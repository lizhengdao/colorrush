﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemProf : MonoBehaviour {
public GameObject[] DownColors;
public GameObject[] MidColors;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void colorChange(string color1, string color2, int nr1, int nr2){
		string Num1;
		string Num2;
		if(nr1 == 0)
			Num1 = "";
		else
			Num1 = "" + nr1;
		if(nr2 == 0)
			Num2 = "";
		else
			Num2 = "" + nr2;
		switch (color1)
		{
			case "blue":
				DownColors[0].SetActive(true);
				var f = DownColors[0].GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				f.text = "" + Num1;
				break;
			case "red":
				DownColors[1].SetActive(true);
				var e = DownColors[1].GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				e.text = "" + Num1;
				break;
			 case "yellow":
				DownColors[2].SetActive(true);
				var d = DownColors[2].GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				d.text = "" + Num1;
				break;
			case "purple":
				DownColors[3].SetActive(true);
				var g = DownColors[3].GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				g.text = "" + Num1;
				break;
			case "orange":
				DownColors[4].SetActive(true);
				var h = DownColors[4].GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				h.text = "" + Num1;
				break;
			case "green":
				DownColors[5].SetActive(true);
				var i = DownColors[5].GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				i.text = "" + Num1;
				break;		
			default:
				break;
		}

		switch (color2)
		{
			case "blue":
				MidColors[0].SetActive(true);
				var f = MidColors[0].GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				f.text = "" + Num2;
				break;
			case "red":
				MidColors[1].SetActive(true);
				var e = MidColors[1].GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				e.text = "" + Num2;
				break;
			 case "yellow":
				MidColors[2].SetActive(true);
				var d = MidColors[2].GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				d.text = "" + Num2;
				break;
			case "purple":
				MidColors[3].SetActive(true);
				var t = MidColors[3].GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				t.text = "" + Num2;
				break;
			case "orange":
				MidColors[4].SetActive(true);
				var s = MidColors[4].GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				s.text = "" + Num2;
				break;
			case "green":
				MidColors[5].SetActive(true);
				var o = MidColors[5].GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				o.text = "" + Num2;
				break;
			default:
				break;
		}
	}

	// public void callOther(string pos){
	// 	string ownNr = this.name.Replace("clone ", "");
	// 	int oNr = int.Parse(ownNr);
	// 	if(oNr%2 == 0){
	// 		GameObject gb = GameObject.Find("clone " + (oNr+1));
	// 		gb.GetComponent<ItemProf>().destroyOther(pos);
	// 	}
	// 	else{
	// 		GameObject gc = GameObject.Find("clone " + (oNr-1));
	// 		gc.GetComponent<ItemProf>().destroyOther(pos);
	// 	}
	// }

	// public void destroyOther(string pos){
	// 	GameObject gb = GameObject.Find(pos);
	// 	gb.GetComponent<Under>().CallDown();
	// }

	public void generateGems(int Nr, int GemNr){
		this.GetComponentInChildren<GenerateGems>().Generate(Nr, GemNr);
	}
}
