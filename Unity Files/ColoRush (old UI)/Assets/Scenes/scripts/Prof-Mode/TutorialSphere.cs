﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialSphere : MonoBehaviour {
public GameObject GameController;
public string color;
public GameObject[] Skins;


void Start () {
		Skins[GameController.GetComponent<TutorialGameController>().SkinNum].SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < Skins.Length; i++)
		{
			Skins[i].SetActive(false);
		}
		Skins[GameController.GetComponent<TutorialGameController>().SkinNum].SetActive(true);
		for (int i = 0; i < Skins.Length; i++)
		{		
			Skins[i].GetComponent<Skins>().changeColor(color);	
		}
	}

	public void red(){
		color = "red";
	}
	
	public void blue(){
		color = "blue";
	}

	public void yellow(){
		color = "yellow";
	}

	public void doubleColor(string C2){
		Debug.Log("doubleColor");
		switch (C2)
		{
			case "blue":
				switch (color)
				{	
					case "red":
						color = "purple";
						GameController.GetComponent<TutorialGameController>().doubled = true;
						break;
					case "yellow":
						color = "green";
						GameController.GetComponent<TutorialGameController>().doubled = true;
						break;
					default:
						color = "blue";
						break;
				}
				break;
			case "red":
				if(color == "yellow"){
					color = "orange";
					GameController.GetComponent<TutorialGameController>().doubled = true;}
				else if(color == "blue"){
					color = "purple";
					GameController.GetComponent<TutorialGameController>().doubled = true;}
				else
					color = "red";
				break;
			case "yellow":
				if(color == "red"){
					color = "orange";
					GameController.GetComponent<TutorialGameController>().doubled = true;}
				else if(color == "blue"){
					color = "green";
					GameController.GetComponent<TutorialGameController>().doubled = true;}
				else
					color = "yellow";
				break;
			default:
				color = C2;
				break;
		}
	}
}
