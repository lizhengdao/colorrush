﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManage : MonoBehaviour
{
    public GameObject Game;
    public GameObject UI;
    public GameControllerMultiplayer GC;
    public ServerClient SC;
    float speed;
    public bool second;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame(){
        if(second)
            SecondStart();
        else{
            speed = GC.speed;
            if(PlayerPrefs.GetFloat("MultiSpeedPref") > 0)
			    speed = PlayerPrefs.GetFloat("MultiSpeedPref");
		    speed = speed + PlayerPrefs.GetFloat("MultiSpeeDifference");
		    if(speed < 0 )
			    speed = 0.5f;
            Debug.Log("GMPref " + PlayerPrefs.GetFloat("MultiSpeedPref"));
            Debug.Log("GMSeeDifference " + PlayerPrefs.GetFloat("MultiSpeeDifference"));
            GC.speed = speed;
            Game.SetActive(true);
            UI.SetActive(false);
            // GC.speedBefore = GC.speed;
            // GC.Reset();
            GC.Pause();
            SC.Game = true;
        }
    }

    public void FinalStart(){
        GC.SendStart();
    }

    public void End(){
        SC.Game = false;
        second = true;
    }

    public void SecondStart(){
        if(PlayerPrefs.GetFloat("MultiSpeedPref") > 0)
			speed = PlayerPrefs.GetFloat("MultiSpeedPref");
		speed = speed + PlayerPrefs.GetFloat("MultiSpeeDifference");
		if(speed < 0 )
			speed = 0.5f;
        Debug.Log("GMPref " + PlayerPrefs.GetFloat("MultiSpeedPref"));
        Debug.Log("GMSeeDifference " + PlayerPrefs.GetFloat("MultiSpeeDifference"));
        GC.speedBefore = speed;
        GC.speed = speed;
        Game.SetActive(true);
        UI.SetActive(false);
        GC.speedBefore = GC.speed;
        GC.Reset();
        GC.Pause();
        SC.Game = true;
    }
}
