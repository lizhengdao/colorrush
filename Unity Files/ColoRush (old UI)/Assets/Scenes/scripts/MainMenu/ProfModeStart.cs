﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ProfModeStart : MonoBehaviour {
public string[] levelNames;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}

	public void LoadLevel(string level)
    {
		PlayerPrefs.SetString("Scene",level);
		SceneManager.LoadScene("Loading");
	}

	public void Quit(){
		Application.Quit();
	}

	public void Randomize(){
		int ran = Random.Range(0,levelNames.Length);
		LoadLevel(levelNames[ran]);
	}

	public void DownloadURL(){
		Application.OpenURL("https://gitlab.com/asdoi/colorrush/blob/master/Apk/ColoRush.apk");
	}

}
