﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System.Text;
using System;
using UnityEditor;

namespace Assets.SimpleAndroidNotifications
{
public class UpdateCheck : MonoBehaviour {

private bool AutoDownload;
public GameObject UpdateObject;
public GameObject AutoDownloadObj;
public GameObject[] uptodate;
public GameObject cancelDown;

public bool cancel = false;

public void Cancel(){
    cancel = true;
}

public void checkUpdate(){
    StartCoroutine(downloadUpdateFile());
}

void Start()
{
    StartCoroutine(downloadUpdateFile());
}

void Update () {
    if(PlayerPrefs.GetInt("Auto") == 1){
        AutoDownload = true;
        AutoDownloadObj.SetActive(true);
    }
    else{
        AutoDownload = false;
        AutoDownloadObj.SetActive(false);
    }
}

public void downloadUpdate(){
    StartCoroutine(downLoadFromServer());
}

public void AutoDown(){
    if(PlayerPrefs.GetInt("Auto") == 1){
        PlayerPrefs.SetInt("Auto" , 0);
    }
    else{
        PlayerPrefs.SetInt("Auto",1);
    }
}

IEnumerator downloadUpdateFile()
{
    string url = "https://gitlab.com/asdoi/colorrush/raw/master/VersionFile.txt?inline=false";

    string savePath = Path.Combine(Application.persistentDataPath, "data");
    savePath = Path.Combine(savePath, "Version.txt");

    Dictionary<string, string> header = new Dictionary<string, string>();
    string userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";
    header.Add("User-Agent", userAgent);
    WWW www = new WWW(url, null, header);


    while (!www.isDone)
    {
        //Must yield below/wait for a frame
        GameObject.Find("TextDebug").GetComponent<Text>().text = "Prüfe auf Update: " + www.progress;
        yield return null;
    }
    

    byte[] yourBytes = www.bytes;

    GameObject.Find("TextDebug").GetComponent<Text>().text = "Done downloading. Size: " + yourBytes.Length;


    //Create Directory if it does not exist
    if (!Directory.Exists(Path.GetDirectoryName(savePath)))
    {
        Directory.CreateDirectory(Path.GetDirectoryName(savePath));
        GameObject.Find("TextDebug").GetComponent<Text>().text = "Created Dir";
    }

    try
    {
        //Now Save it
        System.IO.File.WriteAllBytes(savePath, yourBytes);
        Debug.Log("Saved Data to: " + savePath.Replace("/", "\\"));
        GameObject.Find("TextDebug").GetComponent<Text>().text = "";
    }
    catch (System.Exception e)
    {
        Debug.LogWarning("Failed To Save Data to: " + savePath.Replace("/", "\\"));
        Debug.LogWarning("Error: " + e.Message);
        GameObject.Find("TextDebug").GetComponent<Text>().text = "Error Saving Data";
    }

   
    getUpdate(savePath);
}

private bool getUpdate(string filePath)
{
    
    StreamReader reader = new StreamReader(filePath);

    using(reader)
    {


    try
     {
         string line;
         // Create a new StreamReader, tell it which file to read and what encoding the file
         // was saved as
         StreamReader theReader = new StreamReader(filePath, Encoding.Default);
         // Immediately clean up the reader after this block of code is done.
         // You generally use the "using" statement for potentially memory-intensive objects
         // instead of relying on garbage collection.
         // (Do not confuse this with the using directive for namespace at the 
         // beginning of a class!)
         using (theReader)
         {
             // While there's lines left in the text file, do this:
             do
             {
                 line = theReader.ReadLine();
                     
                 if (line != null)
                 {
                     Debug.Log(line);
                     // Do whatever you need to do with the text line, it's a string now
                     // In this example, I split it into arguments based on comma
                     // deliniators, then send that array to DoStuff()
                     checkUpdate(line);
                     
                 }
             }
             while (line != null);
             // Done reading, close the reader and return true to broadcast success    
             theReader.Close();
             return true;
             }
         }
         // If anything broke in the try block, we throw an exception with information
         // on what didn't work
         catch (System.Exception e)
         {
             Debug.Log(e.Message);
             return false;
         }
}
}


private void checkUpdate(string version)
{
    if(version == Application.version){
        GameObject.Find("TextDebug").GetComponent<Text>().text = "";
        Debug.Log("return");
        for(int i = 0; i < uptodate.Length;i++){
            uptodate[i].SetActive(true);
        }
        return;
    }
    else{
        int pointPos = version.IndexOf('.');
        int appPos = Application.version.IndexOf('.');

        string firstNum = "";
        for(int i = 0; i < pointPos; i++){
            firstNum += version[i];
        }

        string appNum = "";
        for(int i = 0; i < appPos; i++){
            appNum += Application.version[i];
        }

        string lastNum = "";
        for(int i = pointPos+1; i < version.Length; i++){
            lastNum += version[i];
        }

        string applastNum = "";
        for(int i = appPos+1; i < Application.version.Length; i++){
            applastNum += Application.version[i];
        }

        int Num1 = int.Parse(firstNum);
        int Num2 = int.Parse(lastNum);

        int app1 = int.Parse(appNum);
        int app2 = int.Parse(applastNum);

        Debug.Log(Num1 + " " + Num2 + " " + app1 + " " + app2);

        Debug.Log("check");

    if(AutoDownload){
        if(pointPos > appPos)
            StartCoroutine(downLoadFromServer());
        else if(Num1 > app1)
            StartCoroutine(downLoadFromServer());
        else if(Num2 > app2)
            StartCoroutine(downLoadFromServer());
        else{
            return;
        }
        }
    else{
        for(int i = 0; i < uptodate.Length;i++){
            uptodate[i].SetActive(false);
            }
        if(pointPos > appPos){
            UpdateObject.SetActive(true);
            UpdateNotification();
            }
        else if(Num1 > app1){
            UpdateObject.SetActive(true);
            UpdateNotification();
            }
        else if(Num2 > app2){
            UpdateObject.SetActive(true);
            UpdateNotification();
            }
        else{
            for(int i = 0; i < uptodate.Length;i++){
                uptodate[i].SetActive(true);
            }
            return;
        }
        GameObject.Find("TextDebug").GetComponent<Text>().text = "";
    }
    GameObject.Find("TextDebug").GetComponent<Text>().text = "";

    }
}

    void UpdateNotification(){
        var notificationParams = new NotificationParams
            {
                Id = UnityEngine.Random.Range(0, int.MaxValue),
                Delay = TimeSpan.FromSeconds(1),
                Title = "Update Verfügbar",
                Message = "Es ist ein Update Verfügbar",
                Ticker = "Ticker",
                Sound = true,
                Vibrate = true,
                Light = true,
                SmallIcon = NotificationIcon.Message,
                SmallIconColor = new Color(0, 0.5f, 0),
                LargeIcon = "app_icon"
            };

            NotificationManager.SendCustom(notificationParams);
    }

IEnumerator downLoadFromServer()
{
    cancelDown.SetActive(true);
    for(int i = 0; i < uptodate.Length;i++){
            uptodate[i].SetActive(false);
    }
    var notificationParams = new NotificationParams
            {
                Id = UnityEngine.Random.Range(0, int.MaxValue),
                Delay = TimeSpan.FromSeconds(1),
                Title = "Update wird heruntergeladen",
                Message = "Lade Update herunter...",
                Ticker = "Ticker",
                Sound = false,
                Vibrate = true,
                Light = true,
                SmallIcon = NotificationIcon.Message,
                SmallIconColor = new Color(0, 0.5f, 0),
                LargeIcon = "app_icon"
            };

            NotificationManager.SendCustom(notificationParams);


    string url = "https://gitlab.com/asdoi/colorrush/raw/master/Apk/ColoRush.apk";

    string savePath = Path.Combine(Application.persistentDataPath, "data");
    savePath = Path.Combine(savePath, "AntiOvr.apk");

    Dictionary<string, string> header = new Dictionary<string, string>();
    string userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";
    header.Add("User-Agent", userAgent);
    WWW www = new WWW(url, null, header);


    while (!www.isDone)
    {
        //Must yield below/wait for a frame
        GameObject.Find("TextDebug").GetComponent<Text>().text = "Lade herunter: " + www.progress;
        // if (EditorUtility.DisplayCancelableProgressBar("Update verfügbar", "Lade Update herunter", www.progress))
        //         {
        //             Debug.Log("Progress bar canceled by the user");
        //             www.Dispose();
        //         }
        if(cancel){
            www.Dispose();
            GameObject.Find("TextDebug").GetComponent<Text>().text = "";
            cancelDown.SetActive(false);
            cancel = false;
            }
        yield return null;
    }
    byte[] yourBytes = www.bytes;

    GameObject.Find("TextDebug").GetComponent<Text>().text = "Done downloading. Size: " + yourBytes.Length;
    // EditorUtility.ClearProgressBar();

    //Create Directory if it does not exist
    if (!Directory.Exists(Path.GetDirectoryName(savePath)))
    {
        Directory.CreateDirectory(Path.GetDirectoryName(savePath));
        GameObject.Find("TextDebug").GetComponent<Text>().text = "Created Dir";
    }

    try
    {
        //Now Save it
        System.IO.File.WriteAllBytes(savePath, yourBytes);
        Debug.Log("Saved Data to: " + savePath.Replace("/", "\\"));
        GameObject.Find("TextDebug").GetComponent<Text>().text = "";
    }
    catch (System.Exception e)
    {
        Debug.LogWarning("Failed To Save Data to: " + savePath.Replace("/", "\\"));
        Debug.LogWarning("Error: " + e.Message);
        GameObject.Find("TextDebug").GetComponent<Text>().text = "Error Saving Data";
    }

    //Install APK
    installApp(savePath);
}

//For API 24 and above
private bool installApp(string apkPath)
{
    bool success = true;
    GameObject.Find("TextDebug").GetComponent<Text>().text = "Installing App";

    try
    {
        //Get Activity then Context
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject unityContext = currentActivity.Call<AndroidJavaObject>("getApplicationContext");


        //Get the package Name
        string packageName = unityContext.Call<string>("getPackageName");
        string authority = packageName + ".fileprovider";

        AndroidJavaClass intentObj = new AndroidJavaClass("android.content.Intent");
        string ACTION_VIEW = intentObj.GetStatic<string>("ACTION_VIEW");
        AndroidJavaObject intent = new AndroidJavaObject("android.content.Intent", ACTION_VIEW);


        int FLAG_ACTIVITY_NEW_TASK = intentObj.GetStatic<int>("FLAG_ACTIVITY_NEW_TASK");
        int FLAG_GRANT_READ_URI_PERMISSION = intentObj.GetStatic<int>("FLAG_GRANT_READ_URI_PERMISSION");

        //File fileObj = new File(String pathname);
        AndroidJavaObject fileObj = new AndroidJavaObject("java.io.File", apkPath);
        //FileProvider object that will be used to call it static function
        AndroidJavaClass fileProvider = new AndroidJavaClass("android.support.v4.content.FileProvider");
        //getUriForFile(Context context, String authority, File file)
        AndroidJavaObject uri = fileProvider.CallStatic<AndroidJavaObject>("getUriForFile", unityContext, authority, fileObj);

        intent.Call<AndroidJavaObject>("setDataAndType", uri, "application/vnd.android.package-archive");
        intent.Call<AndroidJavaObject>("addFlags", FLAG_ACTIVITY_NEW_TASK);
        intent.Call<AndroidJavaObject>("addFlags", FLAG_GRANT_READ_URI_PERMISSION);
        currentActivity.Call("startActivity", intent);

        GameObject.Find("TextDebug").GetComponent<Text>().text = "";
    }
    catch (System.Exception e)
    {
        GameObject.Find("TextDebug").GetComponent<Text>().text = "Error: " + e.Message;
        success = false;
    }

    return success;
}
}}