﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
 
public class FPSDisplay : MonoBehaviour
{
	float deltaTime = 0.0f;
	public Text thisText;
 
	void Update()
	{
		deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
	}
 
	void OnGUI()
	{
		// int w = Screen.width, h = Screen.height;
 
		// GUIStyle style = new GUIStyle();
 
		// Rect rect = new Rect(0, 0, w, h * 2 / 100);
		// style.alignment = TextAnchor.UpperLeft;
		// style.fontSize = h * 2 / 100;
		// style.normal.textColor = new Color (0.0f, 0.0f, 0.5f, 1.0f);
		float msec = deltaTime * 1000.0f;
		float fps = 1.0f / deltaTime;
		string text = string.Format("{0:0.0} ms \n ({1:0.} fps)", msec, fps);
		// GUI.Label(rect, text, style);
		if(PlayerPrefs.GetInt("FPS") == 1)
			thisText.text = text;
		else{
			thisText.text = "";
		}
	}
}
