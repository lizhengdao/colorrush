﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HardModeScript : MonoBehaviour {
public GameObject Auswahl;
public GameObject Game;

	// Use this for initialization
	void Start () {
		Debug.Log(PlayerPrefs.GetInt("chosen"));
		if(PlayerPrefs.GetInt("chosen") == 1){
			Auswahl.SetActive(false);
			Game.SetActive(true);
		}

	}
	
	// Update is called once per frame
	void Update () {
		if(PlayerPrefs.GetInt("chosen") == 1){
			Auswahl.SetActive(false);
			Game.SetActive(true);
		}
		
	}

	
}
