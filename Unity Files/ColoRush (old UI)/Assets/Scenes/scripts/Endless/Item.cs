﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour {
public GameObject yellowD;
public GameObject redD;
public GameObject blueD;
public GameObject yellowM;
public GameObject redM;
public GameObject blueM;

private GameObject clone;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void colorChange(string color1, string color2, int nr1, int nr2){
		switch (color1)
		{
			 case "yellow":
				yellowD.SetActive(true);
				var d = yellowD.GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				d.text = "" + nr1;
				break;
			case "red":
				redD.SetActive(true);
				var e = redD.GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				e.text = "" + nr1;
				break;
			case "blue":
				Debug.Log("Blue 1");
				blueD.SetActive(true);
				var f = blueD.GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				f.text = "" + nr1;
				break;
			default:
				break;
		}

		switch (color2)
		{
			 case "yellow":
				yellowM.SetActive(true);
				var d = yellowM.GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				d.text = "" + nr2;
				break;
			case "red":
				redM.SetActive(true);
				var e = redM.GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				e.text = "" + nr2;
				break;
			case "blue":
				Debug.Log("Blue 2");
				blueM.SetActive(true);
				var f = blueM.GetComponentInChildren(typeof(TextMesh)) as TextMesh;
				f.text = "" + nr2;
				break;
			default:
				break;
		}
	}
}
