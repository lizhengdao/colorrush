﻿using System.Collections;
using UnityEngine;
using System.IO;
using System.Runtime.InteropServices;


using VoxelBusters;
using VoxelBusters.NativePlugins;

public class share : MonoBehaviour {

	public GameObject GameController;
	private string text;
	public string link;
    public string Mode;

	private bool isSharing = false;

    #if UNITY_IPHONE
	
	[DllImport("__Internal")]
	private static extern void sampleMethod (string iosPath, string message);
	
	[DllImport("__Internal")]
	private static extern void sampleTextMethod (string message);
	
	#endif
	
	public void OnAndroidTextSharingClick()
	{
		GenerateText();
		StartCoroutine(ShareAndroidText());
		
	}

    public void WeiterEmpfehlen(){
        text = "Hey, willst du mit mir ColoRush spielen und zum #ColoRusher werden: ";
        PlayerPrefs.SetInt("Gems", PlayerPrefs.GetInt("Gems")+20);				
        StartCoroutine(ShareAndroidText());
    }

	IEnumerator ShareAndroidText()
	{
		yield return new WaitForEndOfFrame();
		//execute the below lines if being run on a Android device
		#if UNITY_ANDROID
            //Reference of AndroidJavaClass class for intent
            AndroidJavaClass intentClass = new AndroidJavaClass ("android.content.Intent");
            //Reference of AndroidJavaObject class for intent
            AndroidJavaObject intentObject = new AndroidJavaObject ("android.content.Intent");
            //call setAction method of the Intent object created
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            //set the type of sharing that is happening
            intentObject.Call<AndroidJavaObject>("setType", "text/plain");
            //add data to be passed to the other activity i.e., the data to be sent
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), "#ColoRush");
            //intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TITLE"), "Text Sharing ");
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), text + " " + link);
            //get the current activity
            AndroidJavaClass unity = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            //start the activity by sending the intent data
            AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Share Via");
            currentActivity.Call("startActivity", jChooser);
		#endif
	}
	

	public void OniOSTextSharingClick()
	{
		
		#if UNITY_IPHONE || UNITY_IPAD
		string shareMessage = "Wow I Just Share Text ";
		sampleTextMethod (shareMessage);
		
		#endif
	}

	public void RateUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL("market://details?id=YOUR_ID");
		#elif UNITY_IPHONE
		Application.OpenURL("itms-apps://itunes.apple.com/app/idYOUR_ID");
		#endif
	}

    







    public void Source()
    {
        Application.OpenURL("https://gitlab.com/asdoi/colorrush/blob/master/Attribution");
    }
 
    public void ShareSocialMedia ()
    {
        // isSharing = true;
        // StartCoroutine(TakeSSAndShare());
        OnAndroidTextSharingClick();
        PlayerPrefs.SetInt("Gems", PlayerPrefs.GetInt("Gems")+10);	
    }
 
    void LateUpdate ()
    {
        if (isSharing == true)
        {
            isSharing = false;
 
            StartCoroutine (CaptureScreenShoot());
        }
    }
 
    IEnumerator CaptureScreenShoot ()
    {
        yield return new WaitForEndOfFrame ();
 
        Texture2D texture = ScreenCapture.CaptureScreenshotAsTexture ();
 
        ShareSheet(texture);
 
        Object.Destroy (texture);
           
    }
 
    private void ShareSheet (Texture2D texture)
    {
        ShareSheet _shareSheet = new ShareSheet ();
        _shareSheet.Text = text;
        _shareSheet.AttachImage (texture);
        _shareSheet.URL = link;
 
        // NPBinding.Sharing.ShowView (_shareSheet, FinishSharing);
    }
 
    private void FinishSharing (eShareResult _result)
    {
        Debug.Log (_result);
    }

    private IEnumerator TakeSSAndShare()
	{

        
		yield return new WaitForEndOfFrame();

		Texture2D ss = new Texture2D( Screen.width, Screen.height, TextureFormat.RGB24, false );
		ss.ReadPixels( new Rect( 0, 0, Screen.width, Screen.height ), 0, 0 );
		ss.Apply();

		string filePath = Path.Combine( Application.temporaryCachePath, "shared img.png" );
		File.WriteAllBytes( filePath, ss.EncodeToPNG() );
		
		// To avoid memory leaks
		Destroy( ss );

		new NativeShare().AddFile( filePath ).SetSubject("#ColoRusher").SetText(text + " " + link).SetTitle("Teile deinen Score").Share();

		// Share on WhatsApp only, if installed (Android only)
		//if( NativeShare.TargetExists( "com.whatsapp" ) )
		//	new NativeShare().AddFile( filePath ).SetText( "Hello world!" ).SetTarget( "com.whatsapp" ).Share();
	}

    void Start(){
        GenerateText();
        Debug.Log(Mode);
        Debug.Log(text);
    }

    public void GenerateText(){
        int score;
        string gems = "";
        
        switch (Mode)
        {
            case "Running Mode":
                score = GameController.GetComponent<GameControllerChoose>().returnScore();
                gems = ""+GameController.GetComponent<GameControllerChoose>().GemScore;
                break;
            case "Boss Mode":
                score = GameController.GetComponent<GameControllerBoss>().score;
                gems = ""+GameController.GetComponent<GameControllerBoss>().GemScore;
                break;
            case "Prof Mode":
                score = GameController.GetComponent<GameControllerProf>().score;
                gems = ""+GameController.GetComponent<GameControllerProf>().GemScore;
                break;
            case "Endless Mode":
                score = GameController.GetComponent<GameController>().score;
                gems = "";
                break;
            case "Daily2":
                goto case "Daily"; //Fallthrough
            case "Daily":
                score = GameController.GetComponent<GameControllerDy>().score;
                gems = "" + GameController.GetComponent<GameControllerDy>().GemScore;
                break;
            case "Multi":
                score = GameController.GetComponent<GameControllerMultiplayer>().score;
                gems = "" + GameController.GetComponent<GameControllerMultiplayer>().GemScore;
                break;
            default:
                Debug.Log("default");
                score = 0;
                gems = "";
                break;
        }

        if(gems == ""){
            text = "Hey, ich habe " + score + " Punkte in ColoRush erreicht!!! #ColoRusher Versuche mich zu schlagen:";
        }
        else if(Mode == "Daily")
            text = "Hey, ich habe die tägliche Herausforderung in ColoRush gemeistert und dabei " + score + " Punkte erreicht und " + gems + " Diamanten eingesammelt!!! #ColoRusher Versuche mich zu schlagen:";
        else if(Mode == "Daily2")
            text = "Hey, ich habe Level " + PlayerPrefs.GetInt("LevelNum") + " in ColoRush gemeistert und dabei " + score + " Punkte erreicht und " + gems + " Diamanten eingesammelt!!! #ColoRusher Versuche mich zu schlagen:";    
        else
            text = "Hey, ich habe " + score + " Punkte in ColoRush erreicht und " + gems + " Diamanten eingesammelt!!! #ColoRusher Versuche mich zu schlagen:";


    }


}
