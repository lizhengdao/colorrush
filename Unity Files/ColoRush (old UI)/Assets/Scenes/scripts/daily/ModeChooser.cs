﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeChooser : MonoBehaviour {
public bool doubleAward;
public GameObject[] Daily;
public GameObject[] Finished;
public bool turnDailyOff;

	// Use this for initialization
	void Start () {
		string daily = System.DateTime.Today.GetHashCode().ToString();
		if(PlayerPrefs.GetInt("Day") != System.DateTime.Today.GetHashCode()){
			PlayerPrefs.SetInt("Day", System.DateTime.Today.GetHashCode());
			PlayerPrefs.SetInt("FinishedDay", 0);
		}
		if(daily[0] == '-')
			daily = daily.Substring(1, daily.GetHashCode().ToString().Length-1);
		else
			;
		Debug.Log(daily);
		Debug.Log(daily.GetHashCode().ToString());
		// public bool ColorButtons;
		// public bool Running;
		// public int ManyRows;
		// public int length;
		// public int GemAward;
		while(daily.Length < 8){
			if(daily.GetHashCode().ToString()[0] == '-')
				daily += daily.GetHashCode().ToString().Substring(1, daily.GetHashCode().ToString().Length-1);
			else
				daily += daily.GetHashCode().ToString();
		}

		int length = int.Parse(daily[0].ToString()) + int.Parse(daily[1].ToString()) + int.Parse(daily[2].ToString()) + int.Parse(daily[3].ToString());
		Debug.Log("leng " + length);
		Debug.Log("lol " + int.Parse(daily[0].ToString()) + int.Parse(daily[1].ToString()) + int.Parse(daily[2].ToString()) + int.Parse(daily[3].ToString()));
		int GemAward = int.Parse(daily[4].ToString()) + int.Parse(daily[5].ToString());

		while(GemAward < length){
			GemAward += GemAward;
		}

		bool ColorButtons = false;
		bool Running = false;
		if(int.Parse("" + daily[7]) > 5)
			ColorButtons = true;
		else
			Running = true;

		int ManyRows;
		if(ColorButtons){
			int lol = int.Parse(daily[6].ToString());
			if(lol <= 4)
				ManyRows = 3;
			else if(lol <= 7)
				ManyRows = 2;
			else
				ManyRows = 1;
		}
		else
			ManyRows = 3;

		PlayerPrefs.SetInt("DailyManyRows", ManyRows);
		PlayerPrefs.SetInt("DailyGemAward", GemAward);
		PlayerPrefs.SetInt("DailyLength", length);
		if(Running)
			PlayerPrefs.SetInt("DailyRun",1);
		else
			PlayerPrefs.SetInt("DailyRun",2);

		check();

	}
	
	// Update is called once per frame
	void check() {
		if(PlayerPrefs.GetInt("Day") == System.DateTime.Today.GetHashCode() && PlayerPrefs.GetInt("FinishedDay") == 1){
			for (int i = 0; i < Daily.Length; i++)
			{
				if(turnDailyOff)
					Daily[i].SetActive(false);
			}

			for (int i = 0; i < Finished.Length; i++)
			{
				Finished[i].SetActive(true);
			}
		}
		else
		{
			for (int i = 0; i < Daily.Length; i++)
			{
				Daily[i].SetActive(true);
			}

			for (int i = 0; i < Finished.Length; i++)
			{
				Finished[i].SetActive(false);
			}
		}
	}
}
