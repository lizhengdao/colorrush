﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ziel : MonoBehaviour {
public GameObject GameController;
public bool Multi;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other)
	{	
		if(other.gameObject.tag == "sphere"){
				Debug.Log("Ziel erreicht");
				if(Multi)
					GameController.GetComponent<GameControllerMultiplayer>().ZielErreicht();
				else
					GameController.GetComponent<GameControllerDy>().ZielErreicht();
		}
	}
}
